/*  1.Об'єктна модель документа (DOM) – це програмний інтерфейс (API) для HTML і XML документів. 
    DOM надає структуроване подання документа і визначає те, як ця структура може бути доступна з 
    програм, які можуть змінювати вміст, стиль та структуру документа.

    2.innerHTML Повертає всі дочірні елементи, вкладені всередині тега (тег HTML + текстовий вміст).
    innerText Повертає текстовий вміст дочірніх елементів, вкладених усередині мітки.

    3. Є таки звернення до єлементу як:
    - getElementById ()
    - getElementsByName ()
    - getElementsByClassName ()
    - querySelectorAll () - Цей метод є найпоширенішим, так як їм можна замінити будь-який з 
      перерахованих вище методів.
      querySelector ()
     */
            //1
        const p = document.querySelectorAll('p');
        console.log(p);
     
        p.forEach(p => {p.style.backgroundColor = '#ff0000'});

            //2
        const optionsListId = document.querySelector('#optionsList');
        console.log(optionsListId);

        console.log('parentdNode', optionsListId.parentNode);
        console.log('ChildNodes', optionsListId.childNodes);

            //3
        let elem = document.querySelector('#testParagraph');
        console.log(elem);

        elem.textContent = 'This is a paragraph';
    
            //4.5
        const mainHeader = document.querySelector('.main-header');
        
        const child = Array.from(mainHeader.children);

        child.forEach((item) => {
            console.log(mainHeader.children);
            item.classList.add('nav-item');
        });
        

            //4.6
        const section = document.querySelectorAll('section');
        section.forEach(section => {section.classList.remove = '.section-title'});
        console.log(section);

        

  
     

    